"""
testing tassadar
"""
import pytest
from mock import MagicMock, patch
from bot.modules.ws.idee import Idee
from loguru import logger

test_dict = {"een": 1, "twee": 2}


def mocked_in_role(ctx, ws):
    if ctx.role == ws:
        return True
    else:
        return False


def test_get_ws_ws1():
    """
    test_get_ws1, expecting test_dict
    """
    global test_dict
    idee = Idee()
    idee.bot = MagicMock()
    role = MagicMock()
    role.name = "ws1"
    ctx = MagicMock()
    ctx.author.roles = [role]
    logger.info(f"in test ws1 test_dict {test_dict}")
    idee.bot.settings = {"ws1": test_dict}
    ws = idee._get_ws(ctx)
    assert ws == test_dict


def test_get_ws_ws2():
    """
    test_get_ws_ws2, expecting test_dict
    """
    global test_dict
    idee = Idee()
    idee.bot = MagicMock()
    role = MagicMock()
    role.name = "ws2"
    ctx = MagicMock()
    ctx.author.roles = [role]
    logger.info(f"in test ws2 test_dict {test_dict}")
    idee.bot.settings = {"ws2": test_dict}
    ws = idee._get_ws(ctx)
    assert ws == test_dict


def test_get_ws_none():
    """
    test_get_ws_none, expecting None
    """
    idee = Idee()
    idee.bot = MagicMock()
    role = MagicMock()
    role.name = "ws3"
    ctx = MagicMock()
    ctx.author.roles = [role]
    test_dict = {"een": 1, "twee": 2}
    logger.info(f"in test ws3 test_dict {test_dict}")
    idee.bot.settings = {"ws3": test_dict}
    ws = idee._get_ws(ctx)
    assert ws is None

"""
these tests should cover functions and classes in roles.py
"""
import pytest
from mock import AsyncMock, MagicMock
from mock.mock import patch
from bot.modules.utils import get_role_id


def test_get_role_id_ok():
    """
    Testing to get a valid channel id.
    """
    role_name = "testrole"
    role = MagicMock()
    role.name = role_name
    role.id = 7
    ctx = MagicMock()
    ctx.guild.roles = [role]
    res = get_role_id(ctx, role_name)
    assert res == role.id


def test_get_role_id_nok():
    """
    Testing to get a valid channel id.
    """
    role_name = "testchannel_notfound"
    role = MagicMock()
    role.name = "testchannel"
    role.id = 7
    ctx = MagicMock()
    ctx.guild.roles = [role]
    res = get_role_id(ctx, role_name)
    assert res == "role_not_found"

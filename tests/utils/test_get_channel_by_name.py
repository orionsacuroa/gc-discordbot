"""
these tests should cover functions and classes in roles.py
"""
import pytest
from mock import MagicMock
from mock.mock import patch
from bot.modules.utils import get_channel_by_name


def test_get_channel_by_name_ok():
    """
    Testing to get a valid channel id.
    """
    req_channel = "testchannel"
    channel = MagicMock()
    channel.name = req_channel
    channel.id = 7
    ctx = MagicMock()
    ctx.guild.channels = [channel]
    res = get_channel_by_name(ctx, req_channel)
    assert res == channel


def test_get_channel_id_nok():
    """
    Testing to get a valid channel id.
    """
    req_channel = "testchannel_notfound"
    channel = MagicMock()
    channel.name = "testchannel"
    channel.id = 7
    ctx = MagicMock()
    ctx.guild.channels = [channel]
    res = get_channel_by_name(ctx, req_channel)
    assert res == "channel_not_found"

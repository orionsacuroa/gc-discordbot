# discordbot

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=goingcloud.nl_gc-discordbot&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=goingcloud.nl_gc-discordbot)[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=goingcloud.nl_gc-discordbot&metric=coverage)](https://sonarcloud.io/summary/new_code?id=goingcloud.nl_gc-discordbot)

running the discordbot as a container, export your discord token and start docker-compose up.
There are two containers:

- gsheet, this is for updating the database
- bot, that is the actual bot where you interact with.

the data is stored in a sqlite3 database.

dummy commit

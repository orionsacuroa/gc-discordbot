from discord.ext import commands
from loguru import logger
from ..robin import Robin
from ..utils import feedback


class GetAllRoles(Robin):
    """
    The class that contains the Role related functions
    """

    @commands.command(
        name="get_all_roles",
        help="Geeft een overzicht van alle rollen in de guild terug.",
        brief="Geeft een overzicht van alle rollen in de guild terug.",
        hidden="True",
    )
    async def get_all_roles(self, ctx: commands.Context):
        """
        Get a list of all roles in the guild, lined up. (wrapper function)
        """
        guild = ctx.guild
        all_roles = guild.roles
        msg = ""
        for role in all_roles:
            msg += f"role.name: {role.name}\n"
        await feedback(ctx, msg=msg)


def in_role(ctx: commands.Context, req_role: str) -> bool:
    """
    Check if the author of the message is a member of the specified role.

    paramters:
        req_role:        The role where to check membership on.
    """
    for role in ctx.author.roles:
        if role.name == req_role:
            return True
    return False


def get_role_id(ctx: commands.Context, role_name: str):
    """
    Check if the author of the message is a member of the specified role.

    paramters:
        req_role:        The role where to check membership on.
    """
    guild = ctx.guild
    all_roles = guild.roles
    logger.info(f"name in get_role: __{role_name}__")
    for role in all_roles:
        logger.info(f"role.name: __{role.name}__")
        logger.info(f"role.id: {role.id}")
        if role.name == role_name:
            return role.id
    return "role_not_found"


def rolemembers(ctx: commands.Context, role_name: str) -> list:
    """
    Get a list of members of a specified role.

    paramters:
        role_name:        The role where to get the members of.
    """
    members = []
    guild = ctx.guild
    for role in guild.roles:
        if role.name == role_name:
            logger.info(f"found members for {role_name}")
            role_members = role.members
            break
    for role_member in role_members:
        members.append(role_member.id)
    return members

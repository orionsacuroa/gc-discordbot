from discord.ext import commands
from loguru import logger


async def get_channel_id(ctx: commands.Context, channel_name: str):
    """
    Check if the author of the message is a member of the specified role.

    paramters:
        req_role:        The role where to check membership on.
    """
    guild = ctx.guild
    all_channels = guild.channels
    for channel in all_channels:
        if channel.name == channel_name:
            return channel.id
    return "channel_not_found"


def get_channel_by_name(ctx: commands.Context, channel_name: str):
    """
    Check if the author of the message is a member of the specified role.

    paramters:
        req_role:        The role where to check membership on.
    """
    guild = ctx.guild
    all_channels = guild.channels
    for channel in all_channels:
        if channel.name == channel_name:
            return channel
    return "channel_not_found"

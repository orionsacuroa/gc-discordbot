""" This file is for the idea related functions
"""
import os
from discord.ext import commands
from loguru import logger

from ..robin import Robin
from ..utils import sanitize, get_channel_by_name, in_role, get_role_id, feedback


class Idee(Robin):
    """
    The class that contains the Idee functions
    """

    ###############################################################################################
    #  command idee
    ###############################################################################################

    def _get_ws(self, ctx: commands.Context):
        """Get the ws context

        Args:
            ctx (commands.Context): [command context]
        """
        settings = dict(self.bot.settings)
        logger.info(f"settings: {settings}")
        # logger.info(f"testje get {in_role()}")
        if in_role(ctx, "ws1"):
            logger.info("ws1 reached")
            ws = settings.get("ws1")
        elif in_role(ctx, "ws2"):
            logger.info("ws2 reached")
            ws = settings.get("ws2")
        else:
            logger.info("None reached")
            logger.info("None")
            ws = None
        logger.info(f"ws: {ws}")
        return ws

    @commands.command(
        name="idee",
        help=(
            "Als je een idee hebt kun je dit hiermee doorgeven,"
            " de planners krijgen dit in het planning kanaal te zien."
        ),
        brief="Geef een idee door aan de planners.",
    )
    async def idee(self, ctx, *args):
        """
        Posting an idea in the plannnig channel
        """

        if ws := self._get_ws(ctx):
            usermap = self._getusermap(int(ctx.author.id))
            idea = sanitize(" ".join(args), 100)
            officers_channel = get_channel_by_name(ctx, ws.get("officers_channel_name"))
            planner_role_id = get_role_id(ctx, ws.get("planner_role_name"))

            if ctx.channel != get_channel_by_name(ctx, ws.get("channel_name")):
                message = f"{usermap['DiscordAlias']} post je idee in het ws-kanaal"
            else:
                logger.info(f"officers_channel: {officers_channel}")
                message = (
                    f"<@&{planner_role_id}> idee van {usermap['DiscordAlias']}: {idea}"
                )
                await officers_channel.send(message)

                message = f"Dank, {usermap['DiscordAlias']} je is doorgestuurd aan de officers"

        await feedback(ctx=ctx, msg=message, delete_after=3)

FROM --platform=$BUILDPLATFORM python:3-alpine3.15

RUN apk --no-cache upgrade  && \
pip install pipenv
# RUN mkdir bot
WORKDIR /bot


COPY Pipfile /bot
COPY bot/run.py bot/__init__.py /bot/
COPY bot/modules /bot/modules
RUN cd /bot && pipenv install

# ENTRYPOINT python bot.py
